package com.gregskl.haircompare.addhaircut;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.afollestad.materialcamera.MaterialCamera;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gregskl.haircompare.HaircutAdapter;
import com.gregskl.haircompare.MainActivity;
import com.gregskl.haircompare.R;
import com.gregskl.haircompare.VideoPlayerActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;

//TODO use Bundle to save images on recreate
public class MediaAddFragment extends Fragment {

    private final static int CAMERA_RQ = 100;
    private final static int PERMISSION_CAMERA = 1;
    private String mediaPath;

    public MediaAddFragment() {
        // Required empty public constructor
    }

    public static MediaAddFragment newInstance() {
        MediaAddFragment fragment = new MediaAddFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflated = inflater.inflate(R.layout.fragment_media_add, container, false);
        ImageButton addImage = (ImageButton) inflated.findViewById(R.id.add_button);
        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] permissions = new String[] {Manifest.permission.CAMERA};
                boolean allGiven = true;
                for(String permission : permissions) {
                    if(ContextCompat.checkSelfPermission(getActivity(), permission) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(permissions, PERMISSION_CAMERA);
                        allGiven = false;
                        break;
                    }
                }
                if(allGiven)
                    launchCamera();
            }
        });
        return inflated;
    }

    public void launchCamera() {
        new MaterialCamera(this)
            .showPortraitWarning(false)
            .saveDir(getActivity().getCacheDir())
            .audioDisabled(true)
            .start(CAMERA_RQ);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAMERA_RQ) {
            if(resultCode == Activity.RESULT_OK) {
                Toast.makeText(getActivity(), "Saved to: " + data.getDataString(), Toast.LENGTH_LONG).show();
                mediaPath = data.getDataString();
                SharedPreferences.Editor editor = getActivity().getPreferences(Context.MODE_PRIVATE).edit();
                if(getId() == R.id.before_fragment)
                    editor.putString("beforePath", mediaPath);
                else if(getId() == R.id.after_fragment)
                    editor.putString("afterPath", mediaPath);
                editor.apply();
                initMediaView();
            }
        }
    }

    public void initMediaView() {
        ImageView mediaDisplay = getView().findViewById(R.id.media_display);
        RequestOptions options = new RequestOptions()
                .frame(0)
                .centerCrop();
        Glide.with(getContext())
                .load(mediaPath)
                .apply(options)
                .into(mediaDisplay);
        ImageView videoIcon = getView().findViewById(R.id.video_icon);
        videoIcon.setVisibility(View.VISIBLE);
        mediaDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getView().getContext(), VideoPlayerActivity.class);
                intent.putExtra("path", mediaPath);
                startActivity(intent);
            }
        });
    }

    public String getMediaPath() {
        return mediaPath;
    }

    public void setMediaPath(String mediaPath) {
        this.mediaPath = mediaPath;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.i("PermissionStatus", "Start");
        switch (requestCode) {
            case PERMISSION_CAMERA: {
                boolean cameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (grantResults.length > 0 && cameraPermission) {
                    Log.i("PermissionStatus", "Accepted");
                    launchCamera();
                } else {
                    Log.i("PermissionStatus", "Denied");
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
