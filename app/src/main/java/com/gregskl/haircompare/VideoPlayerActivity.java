package com.gregskl.haircompare;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.afollestad.easyvideoplayer.EasyVideoPlayer;

public class VideoPlayerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        EasyVideoPlayer player = findViewById(R.id.player);
        String path = getIntent().getStringExtra("path");
        player.setSource(Uri.parse(path));
    }
}
