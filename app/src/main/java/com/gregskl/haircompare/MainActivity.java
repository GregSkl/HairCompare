package com.gregskl.haircompare;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gregskl.haircompare.addhaircut.HaircutAddActivity;
import com.gregskl.haircompare.settings.SettingsActivity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Haircut> haircuts;
    private HaircutSorter sorter;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private final int ADD_HAIRCUT_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        HaircutIO.setDir(getFilesDir());

        haircuts = new ArrayList<>();
        HaircutIO.load(haircuts);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new HaircutAdapter(haircuts);
        recyclerView.setAdapter(adapter);

        sorter = new HaircutSorter(haircuts, adapter);
        sorter.sortByDate(false);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, HaircutAddActivity.class);
                startActivityForResult(intent, ADD_HAIRCUT_CODE);
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy > 0 && fab.isShown()) {
                    fab.hide();
                }
                else if(dy < 0 && !fab.isShown()) {
                    fab.show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ADD_HAIRCUT_CODE) {
            if(resultCode == RESULT_OK) {
                Haircut haircut = new Haircut(new Date(),
                        data.getStringExtra("hairdresser"),
                        data.getIntExtra("price", 0),
                        data.getStringExtra("currency_type"),
                        data.getFloatExtra("rating", 0.0f));
                haircuts.add(haircut);
                HaircutIO.moveMediaToDir(data.getStringExtra("beforePath"), String.format("%s-before.mp4", haircut.getID()));
                HaircutIO.moveMediaToDir(data.getStringExtra("afterPath"), String.format("%s-after.mp4", haircut.getID()));
                sorter.sortByDate(false);
                HaircutIO.save(haircuts);
                HaircutIO.deleteAllMedia(getCacheDir());
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        else if(id == R.id.action_sort) {
            new MaterialDialog.Builder(this)
                    .title(R.string.dialog_sort)
                    .items(R.array.sort_items)
                    .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                        @Override
                        public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                            switch(which) {
                                case 0:
                                    sorter.sortByDate(true);
                                    break;
                                case 1:
                                    sorter.sortByHairdresser(true);
                                    break;
                                case 2:
                                    sorter.sortByPrice(true);
                                    break;
                                case 3:
                                    sorter.sortByDate(false);
                                    break;
                                case 4:
                                    sorter.sortByHairdresser(false);
                                    break;
                                case 5:
                                    sorter.sortByPrice(false);
                                    break;
                            }
                            return true;
                        }
                    })
                    .show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        HaircutIO.save(haircuts);
    }
}
