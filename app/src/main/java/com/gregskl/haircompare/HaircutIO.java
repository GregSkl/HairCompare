package com.gregskl.haircompare;

import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class HaircutIO {

    public static File dir;
    private static Gson gson = new GsonBuilder().setLenient().create();
    private static final String JSON_FILE_NAME = "data.json";
    private static File dataFile;
    private static Type haircutType = new TypeToken<List<Haircut>>() {}.getType();

    public static void setDir(File directory) {
        dir = directory;
        dataFile = new File(dir, JSON_FILE_NAME);
    }

    public static void load(List<Haircut> haircuts) {
        JsonReader reader;
        try {
            if(!dataFile.exists())
                dataFile.createNewFile();
            reader = new JsonReader(new FileReader(dataFile));
            reader.setLenient(true);
            Collection<? extends Haircut> haircutsJson = (Collection<? extends Haircut>) gson.fromJson(reader, haircutType);
            if(haircutsJson != null)
                haircuts.addAll(haircutsJson);
        } catch(IOException e) {
            e.printStackTrace();
            Log.e("IOFailure", "Read");
        }
    }

    public static void save(List<Haircut> haircuts) {
        try (PrintWriter writer = new PrintWriter(dataFile)) {
            writer.write(gson.toJson(haircuts, haircutType));
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("IOFailure", "Write");
        }
    }

    public static void moveMediaToDir(String path, String newName) {
        try {
            File media = new File(new URI(path));
            File dest = new File(dir, newName);
            if(!media.renameTo(dest)) {
                Log.e("IOFailure", "Move");
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static void deleteMedia(UUID id) {
        for(File file : dir.listFiles()) {
            if(file.getName().contains(id.toString())) {
                file.delete();
            }
        }
    }

    public static void deleteAllMedia(File dir) {
        if(dir.exists()) {
            for (File file : dir.listFiles()) {
                if(!file.isDirectory()) {
                    file.delete();
                }
            }
        }
    }
}
